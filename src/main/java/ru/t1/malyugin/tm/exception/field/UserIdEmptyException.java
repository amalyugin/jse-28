package ru.t1.malyugin.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! UserId is empty...");
    }

}