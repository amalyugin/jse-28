package ru.t1.malyugin.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Role;

@Getter
@Setter
public class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USUAL;

    @NotNull
    private Boolean locked = false;

    @Override
    @NotNull
    public String toString() {
        return String.format("USER %s: %s", id, login);
    }

}