package ru.t1.malyugin.tm.command.data.load;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.data.AbstractDataCommand;
import ru.t1.malyugin.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml";

    @NotNull
    private static final String DESCRIPTION = "Load data from XML";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD XML]");

        @NotNull final String dumpPath = getDumpDir() + FILE_XML;
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(dumpPath));
        @NotNull final String xml = new String(bytes);

        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);

        setDomain(domain);
    }

}